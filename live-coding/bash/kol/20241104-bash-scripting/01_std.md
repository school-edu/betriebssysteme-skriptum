```bash
ls /sepp /home # erzeugt Ausgabe auf stdout und stderr
ls /sepp /home 1> stdout # schreibt stdout in die Datei stdout und stderr weiterhin in die Konsole
ls /sepp /home 2> stderr # schreibt stderr in die Datei stderr und stdout weiterhin in die Konsole
ls /sepp /home 2> logs 1>&2 # schreibt stderr und stdout in die Datei logs
ls /sepp /home 1> logs 2>&1 # schreibt stderr und stdout in die Datei logs
ls /sepp /home 2>&1 1> logs2 # schreibt nur stdout in die Datei logs2
```