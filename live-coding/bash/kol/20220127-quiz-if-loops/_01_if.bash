#!/bin/bash

# Funktioniert folgender Code, um eine Rückmeldung zu bekommen, ob "testdir" existiert und ein Verzeichnis ist?

mkdir testdir

if [ -d testdir ]; then
    echo "existiert!"
fi

rmdir testdir

mkdir testdir

if test -d testdir ; then
    echo "existiert!"
fi

rmdir testdir

touch testdir
# Funktioniert nicht, weil es auch bei Datei Erfolg zurück gibt
if test -e testdir ; then
    echo "existiert!"
fi

rm testdir

mkdir testdir2
cd testdir

if test $? -eq 0 ; then
    echo "existiert!"
fi

rmdir testdir2