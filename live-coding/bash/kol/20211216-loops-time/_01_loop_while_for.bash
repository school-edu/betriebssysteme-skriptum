#!/bin/bash

echo "Start Script"

counter=0

while [ $counter -lt 10 ]; do
    counter=$(expr $counter + 1)
    echo "Hello World ${counter}_!"    
done

echo "For Schleife:"

for ((i=0;i<10;i++)); do
    echo "Hello World $((i+1))_!"  
done