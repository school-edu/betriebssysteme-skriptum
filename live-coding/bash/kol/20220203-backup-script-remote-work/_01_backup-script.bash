#!/bin/bash

# CLI Parameter Zugriff:
echo $0
echo $1
echo $2

date_filename=$(date +%Y%m%d-%H%M%S)
#echo $date_filename

tar -czf /home/ststolz/tmp/backup/${date_filename}_backup.tar /home/ststolz/tmp/files_to_backup

# Delete files older than 1 minute
for i in $(find /home/ststolz/tmp/backup -maxdepth 1 -mmin +1 -type f); do 

    echo "removing file: "$i
    rm $i

done

# Alternate variants:

# find ~/tmp/ -maxdepth 1 -mmin +1 -type f -delete

# find ~/tmp/ -maxdepth 1 -mmin +1 -type f -exec rm {} \;

# Testen, ob der remote Backupfolder vorhanden ist
ssh ststolz@192.168.56.254 test -d /home/ststolz/backupfolder

isFolderRemote=$?

if [ $isFolderRemote -gt 0 ]; then

    echo "Fehler: Remote Backup Folder ist nicht vorhanden!"
    exit 3

fi

for i in $(find /home/ststolz/tmp/backup -maxdepth 1 -type f); do 

    echo "transfer file to remote: "$i
    # rsync anstatt scp, weil nur noch nicht gesicherte Dateien überträgt
    rsync -v --progress $i ststolz@192.168.56.254:backupfolder
    # scp $i ststolz@192.168.56.254:backupfolder

done