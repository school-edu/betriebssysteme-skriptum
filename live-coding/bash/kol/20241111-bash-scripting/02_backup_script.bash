#!/bin/bash

# cp to_backup/01-my-file.md backup_dir/ # 1. Schritt

# Erstellter Dateiname soll mit Dateumsstring anfangen `date +%Y%m%d-%H%M%S`

# Es ist nicht ideal, dass die Backup Verzeichnisse hardcoded sind -> verwende Variablen und am besten Argumente

tar -cf ./backup_dir/archive.tar to_backup/

tar -tvf ./backup_dir/archive.tar

# An dieser Stelle Prüfung auf Rückgabe von tar, ob Fehler. Ansonsten mit echo Fehlermeldung ausgeben!