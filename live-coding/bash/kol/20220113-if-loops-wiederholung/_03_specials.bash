#!/bin/bash

# Einfache Ausgabe
for i in `ls /etc`; do

    echo "Datei: $i"

done

# Hashen jeder Datei
for i in `ls /etc`; do

    sha256sum "/etc/"$i 2> /dev/null

done