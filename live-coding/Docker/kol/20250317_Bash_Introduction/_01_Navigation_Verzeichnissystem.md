# Navigation Verzeichnissystem

Live-Coding:

```bash
# Arbeitsverzeichnis anzeigen
pwd
# Arbeitsverzeichnis zu Home Verzeichnis wechseln (~ ist Kürzel für Home Verzeichnis)
cd ~
# Wieder anzeigen
pwd
# Das gesamte Verzeichnissystem fängt im root Verzeichnis (/) an
ls /
# mit `df` kann ich anzeigen wohin welches Device gemappt wurde
# die Option `-h` steht für human readable
df -h
# Ordner Test unter home Verzeichnis anlegen
cd ~
mkdir test
cd test
echo "Hallo Stefan" > datei1
mkdir next_folder
cd next_folder
# Inhalt der Datei einen Ordner höher ausgeben
cat ../datei1
# Das Selbe noch einmal absolut
cat ~/test/datei1
# oder
cat /home/ststolz/test/datei1
# Usage eines Programmes lesen
ls --help
# `ls [OPTION]... [FILE]...`
# ls -> Executable
# [OPTION] -> An dieser Stelle kommen optional Optionen
# ... -> beliebig viele
# [FILE] -> Hier kann ich Pfad oder Datei angeben
# ... -> beliebig viele
# `ls ~` -> hier ~ = [FILE]
# `ls -lh` hier werden die Optionen l und h verwendet (siehe `ls --help`) - alternative Schreibweise: `ls -l -h` oder `ls -l --human-readable`
user=Stefan
echo "Hallo Stefan"
echo Hallo Stefan
echo 'Hallo Stefan'
echo Hallo user
echo Hallo $user
echo "Hallo $user"
echo 'Hallo $user'
echo "Hallo $userie"
echo "Hallo ${user}ie"
bash
# In dieser Sitzung gibt es die Variable nicht
echo $user
# STRG-D -> eine Sitzung höher (zurück)
export user
bash
# nun gibt es die Variable
echo $user
# Machen weiter bei Verzeichnissystem, .bashrc und PATH Variable hin zu Scripting
```