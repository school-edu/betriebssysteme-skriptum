FROM ubuntu:xenial

RUN [ "apt-get", "update" ]
RUN [ "apt-get", "-y", "upgrade" ]
RUN [ "apt-get", "-y", "install", "iputils-ping" ]

ENTRYPOINT [ "ping", "-c 5", "www.google.de" ]