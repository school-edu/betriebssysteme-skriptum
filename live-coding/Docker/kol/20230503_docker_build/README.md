```bash
# Wiederholung letzte Einheit

sudo docker exec httpd1 bash -c 'echo "Hallo Stefan" > htdocs/index.html'

# Image bauen

## bauen und ausführen eines Images mit `Dockerfile` im selben Verzeichnis

docker build .

docker run --rm [34b4fa67dc04] ls

## bauen des docker bash script executables

docker build -f Dockerfile_02 -t my-script .

## login mit 

docker run --rm -ti my-script bash

## Executable ausführen mit

docker run --rm my-script bash my-script.bash Sepp

## Dockerfile_03 führt das Executable automatisch aus

docker build -f Dockerfile_03 -t my-script .
docker run --rm my-script
```