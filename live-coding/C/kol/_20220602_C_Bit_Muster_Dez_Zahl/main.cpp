#include <stdio.h>

void print_bits(int data){

    for(int i=0; i < 8;i++){
        int zahl_shifted = data >> i;
        int is_bit_set = zahl_shifted & 1;
        printf("%d",is_bit_set);
    }

}

int main(){

    int zahl = 10;
    printf("%d \n",zahl);

    zahl = zahl << 1;
    printf("%d \n",zahl);

    zahl = zahl >> 1;
    printf("%d \n",zahl);

    zahl = 5;

    int maske = 1;

    printf("%d \n",zahl&maske);
    zahl = 5 >> 1;

    printf("%d \n",zahl&maske);

    print_bits(5);

    return 0;
}

