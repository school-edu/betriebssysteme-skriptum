#include <Arduino.h>
#include <Hash.h>

int led_n = 3;
int start_led = 2;

void setup() {
    Serial.begin(9600);
}

void loop() {


    uint8_t hash[20];

    String str_to_hash = "ac";

    // Output simple hex String

    Serial.print("SHA1 Hex Str:");
    Serial.println(sha1(str_to_hash));

    // Produce Byte Array

    const char* data = str_to_hash.c_str();
    
    sha1(&data[0], str_to_hash.length(), &hash[0]);

    // Convert it to String to show that equal
    
    String hashStr = "";
    for(uint16_t i = 0; i < 20; i++) {
        String hex = String(hash[i], HEX);
        if(hex.length() < 2) {
            hex = "0" + hex;
        }
        hashStr += hex;
    }
    Serial.print("SHA1 Hex Str:");
    Serial.println(hashStr);

    Serial.println("-----------------------------");
    
    delay(1000); 
}