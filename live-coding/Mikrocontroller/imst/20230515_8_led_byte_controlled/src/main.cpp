#include <Arduino.h>
#include <Hash.h>

int led_n = 8;
int start_led = 2;

void get_hash(uint8_t data, uint8_t *hash, uint8_t length){
    // Only possible if data only 1 byte
    sha1(&data, 1, &hash[0]);
}

void led_bit_muster(uint8_t val){
    for(uint8_t i = 0; i<8;i++){
        uint8_t led_i = led_n-1+start_led-i;
        digitalWrite(led_i, LOW);  
        uint8_t shifted = val >> i;
        uint8_t is_on = shifted & 1;
        digitalWrite(led_i, is_on);
        Serial.print(is_on);
    }
    Serial.println("");
}

void setup() {
    Serial.begin(9600);
    for(int i = start_led; i < start_led + led_n; i++){
        pinMode(i, OUTPUT);
    }  
}

void loop() {
    uint8_t *hash = new uint8_t[20];
    for(uint8_t i = 0; i < 256; i++){        
        Serial.print("Payment: ");
        Serial.print(i+1);
        Serial.print(" Sat");
        get_hash(i, hash, sizeof(hash));
        Serial.print(" | Pattern: ");
        led_bit_muster(hash[0]);
        delay(3000);      
        
    }
    free(hash);
}

