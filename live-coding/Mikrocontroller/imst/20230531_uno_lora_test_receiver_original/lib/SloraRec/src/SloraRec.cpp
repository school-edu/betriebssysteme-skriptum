#include "Arduino.h"

#include "SloraRec.h"

#include <RadioLib.h>

// SX1272 has different connections:
// NSS pin:   9
// DIO0 pin:  4
// RESET pin: 5
// DIO1 pin:  6

RFM95 radio = new Module(10, 3, 5, 6);

String str;


void setup_slorarec() {      

  // initialize RFM95
  Serial.print(("[RFM95] Initializing ... "));
  int state = radio.begin(868.0, 125.0, 12, 7, 0x34, 20, 8, 1);
  if (state == RADIOLIB_ERR_NONE) {
    Serial.println(("success!"));
  } else {
    Serial.print(("failed, code "));
    Serial.println(state);
    while (true);
  }

}

int receive_with_conf()
{
    Serial.println(("Receive block"));
    int state = radio.receive(str);
    Serial.println(("Received"));

    // you can also receive data as byte array
    /*
    byte byteArr[8];
    int state = radio.receive(byteArr, 8);
    */

    if (state == RADIOLIB_ERR_NONE)
    {
        // packet was successfully received
        Serial.println(("success!"));

        
        // print the data of the packet
        Serial.print(("[SX1278] Data:\t\t\t"));
        Serial.println(str);

        // print the RSSI (Received Signal Strength Indicator)
        // of the last received packet
        Serial.print(("[SX1278] RSSI:\t\t\t"));
        Serial.print(radio.getRSSI());
        Serial.println((" dBm"));

        // print the SNR (Signal-to-Noise Ratio)
        // of the last received packet
        Serial.print(("[SX1278] SNR:\t\t\t"));
        Serial.print(radio.getSNR());
        Serial.println((" dB"));

        // print frequency error
        // of the last received packet
        Serial.print(("[SX1278] Frequency error:\t"));
        Serial.print(radio.getFrequencyError());
        Serial.println((" Hz"));

        
    }
    else if (state == RADIOLIB_ERR_RX_TIMEOUT)
    {
        // timeout occurred while waiting for a packet
        Serial.println(F("timeout!"));
    }
    else if (state == RADIOLIB_ERR_CRC_MISMATCH)
    {
        // packet was received, but is malformed
        Serial.println(("CRC error!"));
    }
    else
    {
        // some other error occurred
        Serial.print(("failed, code "));
        Serial.println(state);
    }
    return state;
}