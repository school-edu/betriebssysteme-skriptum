byte dataPin = 11;
byte stcPin = 8;
byte shcPin = 12;


void setup() {
  pinMode(dataPin, OUTPUT);
  pinMode(stcPin, OUTPUT);
  pinMode(shcPin, OUTPUT);
  digitalWrite(dataPin,LOW);
  digitalWrite(stcPin,LOW);
  digitalWrite(shcPin,LOW);
}

void loop() {
      progShiftReg();
      delay(1000);         
}

void progShiftReg(){
  for(byte i = 0; i < 8; i++){
    if(i % 2 == 0){
        digitalWrite(dataPin,HIGH);
    }
    else{
        digitalWrite(dataPin,LOW);
    }

    digitalWrite(shcPin,HIGH);
    digitalWrite(shcPin,LOW);
  }
  digitalWrite(stcPin,HIGH);
  digitalWrite(stcPin,LOW);
}
