#include <Arduino.h>

int led = LED_BUILTIN;

void setup(){
    // ACHTUNG! Baud Rate muss auch in der platformio.ini mitgeteilt werden, wenn von 9600 abweicht
    Serial.begin(9600);
    pinMode(led,OUTPUT);
}

void loop(){
    digitalWrite(led,HIGH);    
    int value = digitalRead(led);
    Serial.print("HIGH: ");
    Serial.println(value);
    delay(1000);
    digitalWrite(led,LOW);
    value = digitalRead(led);
    Serial.print("LOW: ");
    Serial.println(value);
    delay(1000);
}