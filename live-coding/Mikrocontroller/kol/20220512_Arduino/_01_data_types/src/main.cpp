#include <Arduino.h>
#include <limits.h>

// In C gibt es unsigned, wodurch man den gesamten positiven Bereich ausnutzen kann
unsigned int val;

void setup() {
  Serial.begin(9600);
}

void loop() {
  // Prüfung von unsigned int max value
  Serial.println(UINT_MAX);
  delay(1000);  
}