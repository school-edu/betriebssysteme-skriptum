DROP DATABASE if exists serp;
CREATE DATABASE serp;
USE serp;

DROP TABLE if exists order_details;
DROP TABLE if exists articles;
DROP TABLE if exists orders;
DROP TABLE if exists customers;

CREATE TABLE customers (
    id int NOT NULL auto_increment PRIMARY KEY,
    name varchar(255),
	street varchar(255),
	town varchar(255),
	contact_name varchar(255)
);

CREATE TABLE orders (
    id int NOT NULL auto_increment PRIMARY KEY,
    order_date datetime default now(),
	customers_id int,
	FOREIGN KEY (customers_id) REFERENCES customers(id)
);

CREATE TABLE articles (
    id int NOT NULL auto_increment PRIMARY KEY,
    name varchar(255),
	price DECIMAL(8,2)
);

CREATE TABLE order_details (
    id int NOT NULL auto_increment PRIMARY KEY,
	amount int,
	articles_id int,
	orders_id int,
	FOREIGN KEY (articles_id) REFERENCES articles(id),
	FOREIGN KEY (orders_id) REFERENCES orders(id)
);

INSERT INTO customers Values 
	(1,"Liebmann","Liebmannstr. 23","A-8392 Sagenhaft","Max Mustermann"),
	(2,"Thoning","Thoningstr. 43","A-3819 Maslig","Josefine Musterfrau"),
	(3,"Planberger","Planbergerstr. 83","A-3857 Moosheim","Moritz Semmel");
INSERT INTO orders Values 
	(1,"2019-04-01 00:00:00",1),
	(2,"2019-04-02 00:00:00",1),
	(3,"2019-04-03 00:00:00",2),
	(4,"2019-04-06 00:00:00",2),
	(5,"2019-04-04 00:00:00",2);
INSERT INTO articles Values 
	(1,"FX-8320-E Prozessor",100.00),
	(2,"AUD Radion R9 200 Series",160.00),
	(3,"AsirteK M5A78L-M/USB3",50.00),
	(4,"Klongston 8GB Arbeitsspeicher",42.00),
	(5,"WO Yellow 120GB interne Festplatte",22.00),
	(6,"Su-POWER 550 Watt Netzteil",40.00),
	(7,"EarthCool Xpredator - orange",110.00);
INSERT INTO order_details Values 
	(null,11,3,2),
	(null,13,1,2),
	(null,23,2,1),
	(null,5,5,1),
	(null,27,7,1),
	(null,105,1,3),
	(null,10,7,3),
	(null,22,6,3),
	(null,3,5,3),
	(null,8,7,3);